#!/usr/bin/env bash

# Usage:
# build <generator> <arch> <config> <OS>

# Exit as failure if any command fails
set -e

if [[ -z "$1" || "$1" == "-h" || "$1" == "--help" ]]; then
    echo "$0 <generator> <arch> <config> <OS>"
    exit 0
fi

BASE_DIR="`cd \`dirname ${BASH_SOURCE}\` && pwd`"
SOURCE_DIR="${BASE_DIR}/source"
GENERATOR="$1"
ARCH="$2"
CONFIG="$3"
OS="$4"

if [[ "${GENERATOR}" == "Visual Studio"* ]]; then
    BUILD_DIR="${BASE_DIR}/${GENERATOR} ${ARCH}/${CONFIG}"
    OPTIONS="--toolchain=msvc"
elif [[ "${GENERATOR}" == "Unix Makefiles" ]]; then
    BUILD_DIR="${BASE_DIR}/${OS} ${GENERATOR} ${ARCH}/${CONFIG}"
    OPTIONS="--enable-cross-compile --target-os=linux --arch=${ARCH}"
    # Set rpath (dependency search path) to the location of each executable or shared object to be more like windows.
    export LDFLAGS='-Wl,-rpath=\$$$$ORIGIN' # escaping '$' in make is '$$' and escaping a '$' in bash is '\$'
else
    echo "Unsupported generator"
    exit 1
fi

rm -rf "${BUILD_DIR}"
mkdir -p "${BUILD_DIR}"

if [ "${CONFIG}" == "Debug" ]; then
    OPTIONS="${OPTIONS} --enable-debug=3"
fi

OPTIONS="${OPTIONS} --disable-doc --disable-programs --disable-static"
OPTIONS="${OPTIONS} --enable-shared --prefix='${BUILD_DIR}/install'"

pushd "${BUILD_DIR}"

    echo "Configuring build with ${OPTIONS}"
    echo "./configure ${OPTIONS}" > configure.txt
    eval "${BASE_DIR}/source/configure ${OPTIONS}"
    make -j `nproc` SHELL="sh -ex"
    make install

    if [[ "${GENERATOR}" == "Visual Studio"* ]]; then

        # It looks like the .pdbs will be installed in a future versions of ffmpeg so this won't be needed
        find -mindepth 2 -iname '*.pdb' | xargs -I{} cp {} "${BUILD_DIR}/install/bin/"

        if [[ ! -x "./rcedit.exe" ]]; then
            powershell.exe -Command "Invoke-WebRequest -UseBasicParsing 'https://github.com/electron/rcedit/releases/download/v1.1.1/rcedit-x64.exe' -OutFile rcedit.exe"
        fi

        # This is to address an issue with .msi's where .dll's were not installed due to a perceived file version downgrade (58.91.100.0 -> 0 or Null)
        # Therefor, for this issue, we only care that this version is <= to the version last installed
        ./rcedit.exe ./install/bin/avcodec-58.dll --set-file-version "58.91.100.0"
        ./rcedit.exe ./install/bin/avdevice-58.dll --set-file-version "58.10.100.0"
        ./rcedit.exe ./install/bin/avfilter-7.dll --set-file-version "7.85.100.0"
        ./rcedit.exe ./install/bin/avformat-58.dll --set-file-version "58.45.100.0"
        ./rcedit.exe ./install/bin/avutil-56.dll --set-file-version "56.51.100.0"
        ./rcedit.exe ./install/bin/swresample-3.dll --set-file-version "3.7.100.0"
        ./rcedit.exe ./install/bin/swscale-5.dll --set-file-version "5.7.100.0"
    fi

popd
