**Nuget packaging for FFmpeg Repository**

## Example CMake Usage:
```cmake
target_link_libraries(target avcodec avdevice avformat avutil avfilter swscale swresample)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:avcodec,INTERFACE_INCLUDE_DIRECTORIES>")
```
