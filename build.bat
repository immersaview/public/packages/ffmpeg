@ECHO OFF

REM Usage: build <generator> <arch> <config>

SETLOCAL ENABLEDELAYEDEXPANSION

SET GENERATOR=%1
SET ARCH=%2
SET CONFIG=%3
REM the '.' is to allow an extra '\' to be added for code clarity eg. "!BASE_DIR!\!GENERATOR!"
SET BASE_DIR=%~dp0.

REM remove " and '
SET GENERATOR=%GENERATOR:"=%
SET GENERATOR=%GENERATOR:'=%
SET ARCH=%ARCH:"=%
SET ARCH=%ARCH:'=%

ECHO "Retrieving nuget.exe"
powershell -Command "Invoke-WebRequest https://dist.nuget.org/win-x86-commandline/v5.6.0/nuget.exe -OutFile nuget.exe"
ECHO "Running nuget.exe restore"
nuget.exe restore
ECHO "Removing unistd.h from zconf.h for ffmpeg compilation"
REN "%BASE_DIR%\packages\Imv.External.zlib.1.2.11.20200612\build\native\include\zconf.h" "zconf_old.h"
CALL BatchSubstitute.bat "unistd.h" "stddef.h" "%BASE_DIR%\packages\Imv.External.zlib.1.2.11.20200612\build\native\include\zconf_old.h" > "%BASE_DIR%\packages\Imv.External.zlib.1.2.11.20200612\build\native\include\zconf.h"

ECHO "Renaming static libs to zlib.lib for use with ffmpeg linker"
REN "%BASE_DIR%\packages\Imv.External.zlib.1.2.11.20200612\build\native\windows\vc142\MT\%ARCH%\Debug\zlibstaticd.lib" "zlib.lib"
REN "%BASE_DIR%\packages\Imv.External.zlib.1.2.11.20200612\build\native\windows\vc142\MT\%ARCH%\Release\zlibstatic.lib" "zlib.lib"

REM If you are building locally comment out the following line and then uncomment the subsequent line.
CALL "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" %ARCH%
REM CALL "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\VC\Auxiliary\Build\vcvarsall.bat" %ARCH%

REM adding zlib to the INCLUDE and LIB path so that msvc can detect it and so can 'configure' for ffmpeg
SET "INCLUDE=%INCLUDE%;%BASE_DIR%\packages\Imv.External.zlib.1.2.11.20200612\build\native\include;"
ECHO "INCLUDE:"
ECHO %INCLUDE%
SET "LIB=%LIB%;%BASE_DIR%\packages\Imv.External.zlib.1.2.11.20200612\build\native\windows\vc142\MT\%ARCH%\%CONFIG%;"
ECHO "LIB:"
ECHO %LIB%

CALL C:\msys64\msys2_shell.cmd -here -full-path -no-start -defterm -c -l './build.sh ^"%GENERATOR%^" %ARCH% %CONFIG%' || GOTO :ERROR

:ERROR
ENDLOCAL
EXIT /B %ERRORLEVEL%
